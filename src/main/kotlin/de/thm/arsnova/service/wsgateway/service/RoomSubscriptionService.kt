package de.thm.arsnova.service.wsgateway.service

import de.thm.arsnova.service.wsgateway.event.*
import de.thm.arsnova.service.wsgateway.model.RoomInfo
import kotlinx.coroutines.GlobalScope
import java.util.concurrent.ConcurrentHashMap
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
class RoomSubscriptionService(
		private val messageSender: ScheduledMessageSender,
        private val applicationEventPublisher: ApplicationEventPublisher,
) {
	private val logger = LoggerFactory.getLogger(RoomSubscriptionService::class.java)

	// roomId -> RoomInfo
	private val roomUsers = ConcurrentHashMap<String, RoomInfo>()

	fun addUser(roomId: String, userId: String, role: Int) = GlobalScope.launch {
		var currentUsers: RoomInfo
		var activeChanged: Boolean
		var active: Boolean
		synchronized(roomUsers) {
			currentUsers = roomUsers.getOrPut(roomId) { RoomInfo() }
			active = currentUsers.isActive()
			currentUsers.addUser(userId, role)
			activeChanged = active != currentUsers.isActive()
		}
		sendRoomCountChangedEvent(roomId, currentUsers)
		if (activeChanged) {
			applicationEventPublisher.publishEvent(
				RoomActiveChanged(
					roomId = roomId,
					active = !active,
				)
			)
		}
	}

	fun removeUser(roomId: String, userId: String) = GlobalScope.launch {
		var currentUsers: RoomInfo
		var activeChanged: Boolean
		var active: Boolean
		synchronized(roomUsers) {
			currentUsers = roomUsers.getOrPut(roomId) { RoomInfo() }
			active = currentUsers.isActive()
			currentUsers.removeUser(userId)
			activeChanged = active != currentUsers.isActive()
		}
		sendRoomCountChangedEvent(roomId, currentUsers)
		if (activeChanged) {
			applicationEventPublisher.publishEvent(
				RoomActiveChanged(
					roomId = roomId,
					active = !active,
				)
			)
		}
	}

	fun getRoomCount(roomId: String): RoomCountChanged? {
		return roomUsers[roomId]?.asEvent()
	}

	fun sendRoomCountChangedEvent(roomId: String, roomInfo: RoomInfo) {
		messageSender.addMessage(ScheduledMessage("${roomId}.comment.stream", false) { roomInfo.asEvent() })
	}

	@EventListener
	fun handleRoomUserJoinEvent(event: RoomUserJoinEvent) {
		addUser(event.roomId, event.userId, event.role)
	}

	@EventListener
	fun handleRoomUserLeaveEvent(event: RoomUserLeaveEvent) {
		removeUser(event.roomId, event.userId)
	}
}
