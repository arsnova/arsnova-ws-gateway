# ARSnova WebSocket Gateway

This service provides the WebSocket Gateway for the ARSnova ecosystem.

Starting it: `mvn spring-boot:run`

**Note:** If you want to serve the frontend on an address other than `http://localhost:4200`, adjust the `allowed-origins` variable in application.properties.
