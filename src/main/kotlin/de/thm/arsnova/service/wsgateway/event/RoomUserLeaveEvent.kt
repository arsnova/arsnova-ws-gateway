package de.thm.arsnova.service.wsgateway.event

data class RoomUserLeaveEvent(
		val userId: String,
		val roomId: String,
)
