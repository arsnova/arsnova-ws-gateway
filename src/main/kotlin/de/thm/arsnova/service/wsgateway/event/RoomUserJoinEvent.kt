package de.thm.arsnova.service.wsgateway.event

data class RoomUserJoinEvent(
		val userId: String,
		val roomId: String,
		val role: Int
)
