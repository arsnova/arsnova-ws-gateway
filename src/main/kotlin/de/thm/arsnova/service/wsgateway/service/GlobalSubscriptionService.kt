package de.thm.arsnova.service.wsgateway.service

import de.thm.arsnova.service.wsgateway.event.GlobalCountChanged
import de.thm.arsnova.service.wsgateway.event.GlobalCountUpdateEvent
import de.thm.arsnova.service.wsgateway.event.LivepollActiveChanged
import de.thm.arsnova.service.wsgateway.event.RoomActiveChanged
import de.thm.arsnova.service.wsgateway.event.RoomUserJoinEvent
import de.thm.arsnova.service.wsgateway.event.RoomUserLeaveEvent
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import kotlin.synchronized
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
class GlobalSubscriptionService {

    private val logger = LoggerFactory.getLogger(LivepollSubscriptionService::class.java)

    // userId + roomId -> Int
    private val users = ConcurrentHashMap<String, Int>()
    private var activeRoomCount = AtomicInteger(0)
    private var activeLivepollCount = AtomicInteger(0)
    private var participantCount = 0
    private var moderatorCount = 0
    private var creatorCount = 0
    private var lastSessionCount = 0
    private var changed = AtomicBoolean(false)

    constructor(messageSender: ScheduledMessageSender) {
        messageSender.addMessage(
                ScheduledMessage("global", true) {
                    if (this.changed.getAndSet(false)) getCount() else null
                }
        )
    }

    fun getCount(): GlobalCountChanged {
        return GlobalCountChanged(
                sessionCount = lastSessionCount,
                activeRoomCount = activeRoomCount.get(),
                activeLivepollCount = activeLivepollCount.get(),
                participantCount = participantCount,
                moderatorCount = moderatorCount,
                creatorCount = creatorCount,
        )
    }

    @EventListener
    fun handleGlobalCountUpdateEvent(event: GlobalCountUpdateEvent) {
        this.lastSessionCount = event.wsSessionCount
        this.changed.set(true)
    }

    @EventListener
    fun handleLivepollActiveChangedEvent(event: LivepollActiveChanged) {
        if (event.active) {
            this.activeLivepollCount.incrementAndGet()
        } else {
            this.activeLivepollCount.decrementAndGet()
        }
    }

    @EventListener
    fun handleRoomActiveChangedEvent(event: RoomActiveChanged) {
        if (event.active) {
            this.activeRoomCount.incrementAndGet()
        } else {
            this.activeRoomCount.decrementAndGet()
        }
    }

    @EventListener
    fun handleRoomUserJoinEvent(event: RoomUserJoinEvent) {
        synchronized(users) {
            val key = event.userId + event.roomId
            val current = users[key]
            if (current == event.role) {
                return
            }
            users.put(key, event.role)
            removeEntry(current)
            addEntry(event.role)
        }
        this.changed.set(true)
    }

    @EventListener
    fun handleRoomUserLeaveEvent(event: RoomUserLeaveEvent) {
        synchronized(users) {
            val key = event.userId + event.roomId
            val current = users.remove(key) ?: return
            removeEntry(current)
        }
        this.changed.set(true)
    }

    private fun addEntry(value: Int) {
        if (value == 0) {
            participantCount += 1
        } else if (value == 2) {
            moderatorCount += 1
        } else if (value == 3) {
            creatorCount += 1
        }
    }

    private fun removeEntry(value: Int?) {
        if (value == null) {
            return
        }
        if (value == 0) {
            participantCount -= 1
        } else if (value == 2) {
            moderatorCount -= 1
        } else if (value == 3) {
            creatorCount -= 1
        }
    }
}
