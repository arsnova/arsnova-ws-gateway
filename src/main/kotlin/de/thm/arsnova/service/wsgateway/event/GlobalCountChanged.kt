package de.thm.arsnova.service.wsgateway.event

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(include=As.WRAPPER_OBJECT, use=Id.NAME)
data class GlobalCountChanged(
		val sessionCount: Int,
        val activeRoomCount: Int,
        val activeLivepollCount: Int,
        val participantCount: Int,
        val moderatorCount: Int,
        val creatorCount: Int,
)
