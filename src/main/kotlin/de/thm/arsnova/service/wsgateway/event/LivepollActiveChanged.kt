package de.thm.arsnova.service.wsgateway.event

data class LivepollActiveChanged(
        val livepollId: String,
        val active: Boolean,
)
