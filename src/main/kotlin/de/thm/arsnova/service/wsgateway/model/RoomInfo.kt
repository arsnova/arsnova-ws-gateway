package de.thm.arsnova.service.wsgateway.model

import de.thm.arsnova.service.wsgateway.event.RoomCountChanged

class RoomInfo {

    private var participantCount = 0
    private var moderatorCount = 0
    private var creatorCount = 0
    private val users = HashMap<String, Int>()

    fun addUser(name: String, role: Int) {
        removeEntry(users.put(name, role))
        if (role == 0) {
            participantCount += 1
        } else if (role == 2) {
            moderatorCount += 1
        } else if (role == 3) {
            creatorCount += 1
        }
    }

    fun removeUser(name: String) {
        removeEntry(users.remove(name))
    }

    fun isActive(): Boolean {
        return (moderatorCount > 0 || creatorCount > 0) && participantCount >= 5
    }

	fun asEvent(): RoomCountChanged {
		return RoomCountChanged(
			participantCount,
			moderatorCount,
			creatorCount
		)
	}

    private fun removeEntry(value: Int?) {
        if (value == null) {
            return
        }
        if (value == 0) {
            participantCount -= 1
        } else if (value == 2) {
            moderatorCount -= 1
        } else if (value == 3) {
            creatorCount -= 1
        }
    }
}
