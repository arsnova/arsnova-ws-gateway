package de.thm.arsnova.service.wsgateway.controller

import de.thm.arsnova.service.wsgateway.service.LivepollSubscriptionService
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController("LivepollSubscriptionController")
class LivepollSubscriptionController(
		private val livepollSubscriptionService: LivepollSubscriptionService
) {
	companion object {
		const val LIVEPOLL_SUBSCRIPTION_MAPPING = "/livepollsubscription"
		const val GET_USER_COUNT_SUBSCRIPTION = "${LIVEPOLL_SUBSCRIPTION_MAPPING}/usercount"
	}

	private val logger = LoggerFactory.getLogger(LivepollSubscriptionController::class.java)

	@GetMapping(GET_USER_COUNT_SUBSCRIPTION)
	fun getUserCount(
			@RequestParam ids: List<String>
	): List<Int?> {
		return ids.map { id -> livepollSubscriptionService.getUserCount(id) }
	}
}
