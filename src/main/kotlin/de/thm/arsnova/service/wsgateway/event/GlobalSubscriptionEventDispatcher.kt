package de.thm.arsnova.service.wsgateway.event

import java.util.concurrent.ConcurrentHashMap
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.stereotype.Component
import org.springframework.web.socket.messaging.SessionDisconnectEvent
import org.springframework.web.socket.messaging.SessionSubscribeEvent
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent

@Component
class GlobalSubscriptionEventDispatcher(
        private val applicationEventPublisher: ApplicationEventPublisher,
) {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val globalTopicPattern = Regex("^/topic/global$")
    private val wsSessionIdToSubscriptionMapping = ConcurrentHashMap<String, String>()

    fun getWsSessionCount(): Int {
        return wsSessionIdToSubscriptionMapping.size
    }

    @EventListener
    fun dispatchSubscribeEvent(event: SessionSubscribeEvent) {
        val accessor = StompHeaderAccessor.wrap(event.message)
        globalTopicPattern.find(accessor.destination!!) ?: return
        synchronized(wsSessionIdToSubscriptionMapping) {
            val oldRoomSubscription = wsSessionIdToSubscriptionMapping[accessor.sessionId]
            if (oldRoomSubscription != null) {
                return
            }
            wsSessionIdToSubscriptionMapping[accessor.sessionId!!] = accessor.subscriptionId!!
            applicationEventPublisher.publishEvent(
                    GlobalCountUpdateEvent(
                            wsSessionCount = wsSessionIdToSubscriptionMapping.count()
                    )
            )
        }
    }

    @EventListener
    fun dispatchUnsubscribeEvent(event: SessionUnsubscribeEvent) {
        val accessor = StompHeaderAccessor.wrap(event.message)
        synchronized(wsSessionIdToSubscriptionMapping) {
            val globalSubscriptionId = wsSessionIdToSubscriptionMapping[accessor.sessionId]
            if (globalSubscriptionId != accessor.subscriptionId) {
                return
            }
            wsSessionIdToSubscriptionMapping.remove(accessor.sessionId)
            applicationEventPublisher.publishEvent(
                    GlobalCountUpdateEvent(
                            wsSessionCount = wsSessionIdToSubscriptionMapping.count()
                    )
            )
        }
    }

    @EventListener
    fun dispatchDisconnectEvent(event: SessionDisconnectEvent) {
        val accessor = StompHeaderAccessor.wrap(event.message)
        synchronized(wsSessionIdToSubscriptionMapping) {
            wsSessionIdToSubscriptionMapping.remove(accessor.sessionId) ?: return
            applicationEventPublisher.publishEvent(
                    GlobalCountUpdateEvent(
                            wsSessionCount = wsSessionIdToSubscriptionMapping.count()
                    )
            )
        }
    }
}
