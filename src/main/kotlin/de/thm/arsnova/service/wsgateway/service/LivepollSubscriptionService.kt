package de.thm.arsnova.service.wsgateway.service

import de.thm.arsnova.service.wsgateway.event.LivepollActiveChanged
import de.thm.arsnova.service.wsgateway.event.LivepollUserJoinEvent
import de.thm.arsnova.service.wsgateway.event.LivepollUserLeaveEvent
import de.thm.arsnova.service.wsgateway.event.UserCountChanged
import de.thm.arsnova.service.wsgateway.model.LivepollInfo
import java.util.concurrent.ConcurrentHashMap
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
class LivepollSubscriptionService(
        private val messageSender: ScheduledMessageSender,
        private val applicationEventPublisher: ApplicationEventPublisher,
) {
    private val logger = LoggerFactory.getLogger(LivepollSubscriptionService::class.java)

    // livepoll -> LivepollInfo
    private val livepollUsers = ConcurrentHashMap<String, LivepollInfo>()

    fun addUser(livepollId: String, userId: String, role: Int) =
            GlobalScope.launch {
                var currentUsers: LivepollInfo
                var prevSize: Int
                synchronized(livepollUsers) {
                    currentUsers = livepollUsers.getOrPut(livepollId) { LivepollInfo() }
                    prevSize = currentUsers.count()
                    currentUsers.addUser(userId, role)
                }
                sendUserCountChangedEvent(livepollId, currentUsers.count())
                if (prevSize < 1 && currentUsers.count() > 0) {
                    applicationEventPublisher.publishEvent(
                            LivepollActiveChanged(livepollId = livepollId, active = true)
                    )
                }
            }

    fun removeUser(livepollId: String, userId: String) =
            GlobalScope.launch {
                var currentUsers: LivepollInfo
                var prevSize: Int
                synchronized(livepollUsers) {
                    currentUsers = livepollUsers.getOrPut(livepollId) { LivepollInfo() }
                    prevSize = currentUsers.count()
                    currentUsers.removeUser(userId)
                }
                sendUserCountChangedEvent(livepollId, currentUsers.count())
                if (prevSize > 0 && currentUsers.count() < 1) {
                    applicationEventPublisher.publishEvent(
                            LivepollActiveChanged(livepollId = livepollId, active = false)
                    )
                }
            }

    fun getUserCount(livepollId: String): Int? {
        return livepollUsers.get(livepollId)?.count()
    }

    fun sendUserCountChangedEvent(livepollId: String, currentUserCount: Int) {
        messageSender.addMessage(
                ScheduledMessage("${livepollId}.livepoll.stream", false) {
                    UserCountChanged(currentUserCount)
                }
        )
    }

    @EventListener
    fun handleLivepollUserJoinEvent(event: LivepollUserJoinEvent) {
        addUser(event.livepollId, event.userId, event.role)
    }

    @EventListener
    fun handleLivepollUserLeaveEvent(event: LivepollUserLeaveEvent) {
        removeUser(event.livepollId, event.userId)
    }
}
