package de.thm.arsnova.service.wsgateway.event

data class LivepollUserJoinEvent(
		val userId: String,
		val livepollId: String,
		val role: Int
)
