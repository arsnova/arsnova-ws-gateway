package de.thm.arsnova.service.wsgateway.event

import de.thm.arsnova.service.wsgateway.model.LivepollSubscription
import java.util.concurrent.ConcurrentHashMap
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.stereotype.Component
import org.springframework.web.socket.messaging.SessionDisconnectEvent
import org.springframework.web.socket.messaging.SessionSubscribeEvent
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent

@Component
class LivepollSubscriptionEventDispatcher(
                private val applicationEventPublisher: ApplicationEventPublisher,
) {
        private val logger = LoggerFactory.getLogger(this::class.java)
        private val livepollTopicPattern =
                        Regex(
                                        "^/topic/([0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12})\\.livepoll\\.stream$"
                        )
        private val wsSessionIdToSubscriptionMapping =
                        ConcurrentHashMap<String, LivepollSubscription>()

        // (userId, livepoll) -> Set<wsSessionIds>
        private val userLivepollSessionIds = ConcurrentHashMap<String, MutableSet<String>>()

        fun getWsSessionCount(): Int {
                return wsSessionIdToSubscriptionMapping.size
        }

        @EventListener
        fun dispatchSubscribeEvent(event: SessionSubscribeEvent) {
                val accessor = StompHeaderAccessor.wrap(event.message)
                val result = livepollTopicPattern.find(accessor.destination!!) ?: return
                val livepollId = result.groups[1]!!.value
                val userId = accessor.getFirstNativeHeader("ars-user-id") ?: return
                val userRole = accessor.getFirstNativeHeader("ars-role") ?: return
                val livepollSubscription: LivepollSubscription
                synchronized(wsSessionIdToSubscriptionMapping) {
                        val oldRoomSubscription =
                                        wsSessionIdToSubscriptionMapping[accessor.sessionId]
                        if (oldRoomSubscription != null) {
                                applicationEventPublisher.publishEvent(
                                                LivepollLeaveEvent(
                                                                wsSessionId = accessor.sessionId!!,
                                                                userId = oldRoomSubscription.userId,
                                                                livepollId =
                                                                                oldRoomSubscription
                                                                                                .livepollId,
                                                )
                                )
                        }
                        livepollSubscription =
                                        LivepollSubscription(
                                                        subscriptionId = accessor.subscriptionId!!,
                                                        userId = userId,
                                                        livepollId = livepollId,
                                        )
                        logger.debug(
                                        "Adding Livepoll WS session -> subscription mapping: {} -> {}, ",
                                        accessor.sessionId,
                                        livepollSubscription
                        )
                        wsSessionIdToSubscriptionMapping[accessor.sessionId!!] =
                                        livepollSubscription
                        applicationEventPublisher.publishEvent(
                                        LivepollJoinEvent(
                                                        wsSessionId = accessor.sessionId!!,
                                                        userId = livepollSubscription.userId,
                                                        livepollId =
                                                                        livepollSubscription
                                                                                        .livepollId,
                                                        role = userRole.toInt(),
                                        )
                        )
                }
        }

        @EventListener
        fun dispatchUnsubscribeEvent(event: SessionUnsubscribeEvent) {
                val accessor = StompHeaderAccessor.wrap(event.message)
                synchronized(wsSessionIdToSubscriptionMapping) {
                        val livepollSubscription =
                                        wsSessionIdToSubscriptionMapping[accessor.sessionId]
                        if (livepollSubscription == null ||
                                                        accessor.subscriptionId !=
                                                                        livepollSubscription
                                                                                        .subscriptionId
                        ) {
                                return
                        }
                        logger.debug(
                                        "Removing livepoll WS session -> subscription mapping: {} -> {}, ",
                                        accessor.sessionId,
                                        livepollSubscription
                        )
                        wsSessionIdToSubscriptionMapping.remove(accessor.sessionId)
                        applicationEventPublisher.publishEvent(
                                        LivepollLeaveEvent(
                                                        wsSessionId = accessor.sessionId!!,
                                                        userId = livepollSubscription.userId,
                                                        livepollId =
                                                                        livepollSubscription
                                                                                        .livepollId,
                                        )
                        )
                }
        }

        @EventListener
        fun dispatchDisconnectEvent(event: SessionDisconnectEvent) {
                val accessor = StompHeaderAccessor.wrap(event.message)
                synchronized(wsSessionIdToSubscriptionMapping) {
                        val livepollSubscription =
                                        wsSessionIdToSubscriptionMapping[accessor.sessionId]
                                                        ?: return
                        logger.debug(
                                        "Removing livepoll WS session -> subscription mapping: {} -> {}, ",
                                        accessor.sessionId,
                                        livepollSubscription
                        )
                        wsSessionIdToSubscriptionMapping.remove(accessor.sessionId)
                        applicationEventPublisher.publishEvent(
                                        LivepollLeaveEvent(
                                                        wsSessionId = accessor.sessionId!!,
                                                        userId = livepollSubscription.userId,
                                                        livepollId =
                                                                        livepollSubscription
                                                                                        .livepollId,
                                        )
                        )
                }
        }

        @EventListener
        fun dispatchLivepollLeaveEvent(event: LivepollLeaveEvent) {
                val userLivepollId = event.userId + event.livepollId
                synchronized(userLivepollSessionIds) {
                        val sessions = userLivepollSessionIds[userLivepollId] ?: return
                        sessions.remove(event.wsSessionId)
                        if (sessions.size > 0) {
                                return
                        }
                        userLivepollSessionIds.remove(userLivepollId)
                }
                applicationEventPublisher.publishEvent(
                                LivepollUserLeaveEvent(
                                                userId = event.userId,
                                                livepollId = event.livepollId
                                )
                )
        }

        @EventListener
        fun dispatchLivepollJoinEvent(event: LivepollJoinEvent) {
                val userLivepollId = event.userId + event.livepollId
                synchronized(userLivepollSessionIds) {
                        val sessions =
                                        userLivepollSessionIds.getOrPut(userLivepollId) {
                                                mutableSetOf()
                                        }
                        val sessionsCountBefore = sessions.size
                        sessions.add(event.wsSessionId)
                        if (sessionsCountBefore > 0) {
                                return
                        }
                }
                var userRole = if (event.role < 0 || event.role > 3) 0 else event.role
                if (userRole == 1) {
                        userRole = 2
                }
                applicationEventPublisher.publishEvent(
                                LivepollUserJoinEvent(
                                                userId = event.userId,
                                                livepollId = event.livepollId,
                                                role = userRole,
                                )
                )
        }
}
