package de.thm.arsnova.service.wsgateway.event

data class LivepollJoinEvent(
		val wsSessionId: String,
		val userId: String,
		val livepollId: String,
		val role: Int
)
