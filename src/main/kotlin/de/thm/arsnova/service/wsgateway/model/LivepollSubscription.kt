package de.thm.arsnova.service.wsgateway.model

data class LivepollSubscription(val subscriptionId: String, val userId: String, val livepollId: String)