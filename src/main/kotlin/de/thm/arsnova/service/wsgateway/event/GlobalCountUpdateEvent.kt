package de.thm.arsnova.service.wsgateway.event

data class GlobalCountUpdateEvent(
		val wsSessionCount: Int,
)
