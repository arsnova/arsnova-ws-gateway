package de.thm.arsnova.service.wsgateway.event

data class LivepollLeaveEvent(
		val wsSessionId: String,
		val userId: String,
		val livepollId: String,
)
