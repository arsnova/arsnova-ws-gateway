package de.thm.arsnova.service.wsgateway.event

data class RoomActiveChanged(
        val roomId: String,
        val active: Boolean,
)
