package de.thm.arsnova.service.wsgateway.event

import de.thm.arsnova.service.wsgateway.model.RoomSubscription
import java.util.concurrent.ConcurrentHashMap
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.stereotype.Component
import org.springframework.web.socket.messaging.SessionDisconnectEvent
import org.springframework.web.socket.messaging.SessionSubscribeEvent
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent

@Component
class RoomSubscriptionEventDispatcher(
        private val applicationEventPublisher: ApplicationEventPublisher,
) {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val roomTopicPattern =
            Regex(
                    "^/topic/([0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12})\\.comment\\.stream$"
            )
    private val wsSessionIdToSubscriptionMapping = ConcurrentHashMap<String, RoomSubscription>()

    // (userId, roomId) -> Set<wsSessionIds>
    private val userRoomSessionIds = ConcurrentHashMap<String, MutableSet<String>>()

    fun getWsSessionCount(): Int {
        return wsSessionIdToSubscriptionMapping.size
    }

    @EventListener
    fun dispatchSubscribeEvent(event: SessionSubscribeEvent) {
        val accessor = StompHeaderAccessor.wrap(event.message)
        logger.trace("Handling session subscribe event: {}", accessor)
        val result = roomTopicPattern.find(accessor.destination!!) ?: return
        val roomId = result.groups[1]!!.value
        val userId = accessor.getFirstNativeHeader("ars-user-id") ?: return
        val userRole = accessor.getFirstNativeHeader("ars-role") ?: return
        val roomSubscription: RoomSubscription
        synchronized(wsSessionIdToSubscriptionMapping) {
            val oldRoomSubscription = wsSessionIdToSubscriptionMapping[accessor.sessionId]
            if (oldRoomSubscription != null) {
                applicationEventPublisher.publishEvent(
                        RoomLeaveEvent(
                                wsSessionId = accessor.sessionId!!,
                                userId = oldRoomSubscription.userId,
                                roomId = oldRoomSubscription.roomId,
                        )
                )
            }
            roomSubscription =
                    RoomSubscription(
                            subscriptionId = accessor.subscriptionId!!,
                            userId = userId,
                            roomId = roomId,
                    )
            logger.debug(
                    "Adding WS session -> subscription mapping: {} -> {}, ",
                    accessor.sessionId,
                    roomSubscription
            )
            wsSessionIdToSubscriptionMapping[accessor.sessionId!!] = roomSubscription
            applicationEventPublisher.publishEvent(
                    RoomJoinEvent(
                            wsSessionId = accessor.sessionId!!,
                            userId = roomSubscription.userId,
                            roomId = roomSubscription.roomId,
                            role = userRole.toInt(),
                    )
            )
        }
    }

    @EventListener
    fun dispatchUnsubscribeEvent(event: SessionUnsubscribeEvent) {
        val accessor = StompHeaderAccessor.wrap(event.message)
        logger.trace("Handling session unsubscribe event: {}", accessor)
        synchronized(wsSessionIdToSubscriptionMapping) {
            val roomSubscription = wsSessionIdToSubscriptionMapping[accessor.sessionId]
            if (roomSubscription == null ||
                            accessor.subscriptionId != roomSubscription.subscriptionId
            ) {
                return
            }
            logger.debug(
                    "Removing WS session -> subscription mapping: {} -> {}, ",
                    accessor.sessionId,
                    roomSubscription
            )
            wsSessionIdToSubscriptionMapping.remove(accessor.sessionId)
            applicationEventPublisher.publishEvent(
                    RoomLeaveEvent(
                            wsSessionId = accessor.sessionId!!,
                            userId = roomSubscription.userId,
                            roomId = roomSubscription.roomId,
                    )
            )
        }
    }

    @EventListener
    fun dispatchDisconnectEvent(event: SessionDisconnectEvent) {
        val accessor = StompHeaderAccessor.wrap(event.message)
        logger.trace("Handling session disconnect event: {}", accessor)
        synchronized(wsSessionIdToSubscriptionMapping) {
            val roomSubscription = wsSessionIdToSubscriptionMapping[accessor.sessionId] ?: return
            logger.debug(
                    "Removing WS session -> subscription mapping: {} -> {}, ",
                    accessor.sessionId,
                    roomSubscription
            )
            wsSessionIdToSubscriptionMapping.remove(accessor.sessionId)
            applicationEventPublisher.publishEvent(
                    RoomLeaveEvent(
                            wsSessionId = accessor.sessionId!!,
                            userId = roomSubscription.userId,
                            roomId = roomSubscription.roomId,
                    )
            )
        }
    }

    @EventListener
    fun dispatchRoomLeaveEvent(event: RoomLeaveEvent) {
        val userRoomId = event.userId + event.roomId
        synchronized(userRoomSessionIds) {
            val sessions = userRoomSessionIds[userRoomId] ?: return
            sessions.remove(event.wsSessionId)
            if (sessions.size > 0) {
                return
            }
            userRoomSessionIds.remove(userRoomId)
        }
        applicationEventPublisher.publishEvent(
                RoomUserLeaveEvent(userId = event.userId, roomId = event.roomId)
        )
    }

    @EventListener
    fun dispatchRoomJoinEvent(event: RoomJoinEvent) {
        val userRoomId = event.userId + event.roomId
        synchronized(userRoomSessionIds) {
            val sessions = userRoomSessionIds.getOrPut(userRoomId) { mutableSetOf() }
            val sessionsCountBefore = sessions.size     
            sessions.add(event.wsSessionId)
            if (sessionsCountBefore > 0) {
                return
            }
        }
        var userRole = if (event.role < 0 || event.role > 3)  0 else event.role
        if (userRole == 1) {
                userRole = 2
        }
        applicationEventPublisher.publishEvent(
                RoomUserJoinEvent(
                        userId = event.userId,
                        roomId = event.roomId,
                        role = userRole,
                )
        )
    }
}
