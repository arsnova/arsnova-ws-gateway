package de.thm.arsnova.service.wsgateway.event

data class LivepollUserLeaveEvent(
		val userId: String,
		val livepollId: String,
)
