package de.thm.arsnova.service.wsgateway.service

import org.slf4j.LoggerFactory
import kotlin.synchronized
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import de.thm.arsnova.service.wsgateway.config.WebSocketProperties

class ScheduledMessage(val target: String, val persistent: Boolean, val toSend: () -> Any?);

@Service
class ScheduledMessageSender(
        private val rabbitTemplate: RabbitTemplate,
        private val webSocketProperties: WebSocketProperties
) {
    private val logger = LoggerFactory.getLogger(ScheduledMessageSender::class.java)
    private val messages: MutableMap<String, ScheduledMessage> = HashMap()
    private val counterReset = webSocketProperties.gateway.eventRateLimit.sendInterval
    private var counter = counterReset

    fun addMessage(message: ScheduledMessage) {
        synchronized(messages) {
            messages.put(message.target, message)
        }
    }

    @Scheduled(fixedDelay = 50)
    protected fun send() {
        if (--counter > 0) {
            return
        }
        counter = counterReset;
        synchronized(messages) {
            with (messages.iterator()) {
                forEach { (_, value) -> 
                    val obj = value.toSend()
                    if (obj != null) {
                        sendMessage(value.target, obj)
                    }
                    if (!value.persistent) {
                        remove()
                    }
                }
            }
        }
    }

    private fun sendMessage(routingKey: String, obj: Any) {
        rabbitTemplate.convertAndSend("amq.topic", routingKey, obj)
    }
}
